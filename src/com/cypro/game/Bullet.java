package com.cypro.game;

import com.cypro.tank.Tank;
import com.cypro.util.Constant;
import sun.net.www.content.text.Generic;

import java.awt.*;

/**
 * 炮弹类
 */
public class Bullet {
    //炮弹默认速度是坦克速度的2倍
    public static final int DEFAULT_SPEED = Tank.DEFAULT_SPEED << 1;
    //炮弹速度
    public static final int RADIUS = 4;
    private int x, y;
    private int speed = DEFAULT_SPEED;
    private int dir;
    private int atk;
    private Color color;
    //子弹是否可见（飞到屏幕外了）
    private Boolean visible = true;

    /**
     * 给对象池使用的，所有的属性都是默认值
     */
    public Bullet(){}

    public Bullet(int x, int y, int dir, int atk, Color color) {
        this.x = x;
        this.y = y;
        this.dir = dir;
        this.atk = atk;
        this.color = color;
    }

    /**
     * 绘制炮弹
     * @param g
     */
    public void draw(Graphics g) {
        if (!visible) return;
        logic();
        g.setColor(color);
        g.fillOval(x, y, RADIUS << 1, RADIUS << 1);
    }

    /**
     * 炮弹的逻辑
     */
    private void logic(){
        move();
    }

    private void move() {
        switch (dir) {
            case Tank.DIR_UP:
                y -= speed;
                visible = y > 0;
                break;
            case Tank.DIR_DOWN:
                y += speed;
                visible = y < Constant.FRAME_HEIGHT;
                break;
            case Tank.DIR_LEFT:
                x -= speed;
                visible = x > 0;
                break;
            case Tank.DIR_RIGHT:
                x += speed;
                visible = x < Constant.FRAME_WIDTH;
                break;
        }
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getDir() {
        return dir;
    }

    public void setDir(int dir) {
        this.dir = dir;
    }

    public int getAtk() {
        return atk;
    }

    public void setAtk(int atk) {
        this.atk = atk;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }
}
