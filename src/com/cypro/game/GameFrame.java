package com.cypro.game;

import com.cypro.tank.EnemyTank;
import com.cypro.tank.MyTank;
import com.cypro.tank.Tank;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import static com.cypro.util.Constant.*;

/**
 * 游戏的主窗口类
 * 所有游戏展示的内容都要在该类中实现
 */
public class GameFrame extends Frame implements Runnable{
    //第一步：定义一张和屏幕大小一致的图片
    private BufferedImage bufImg = new BufferedImage(FRAME_WIDTH, FRAME_HEIGHT, BufferedImage.TYPE_4BYTE_ABGR);
    //游戏状态
    public static int gameState;
    //菜单指向(0~4)
    private int menuIndex;

    //定义坦克对象
    private Tank myTank;

    //敌人的坦克容器
    private List<Tank> enemies = new ArrayList<>();

    //标题栏高度
    public static int titleBarH;

    /**
     * 对窗口进行初始化
     */
    public GameFrame () {
        initFrame();
        initListener();
        //启动用于刷新窗口的线程
        new Thread(this).start();
    }

    /**
     * 对游戏进行初始化
     */
    private void initGame() {
        gameState = STATUS_MENU;
    }

    /**
     * 属性初始化
     */
    private void initFrame() {
        setTitle(GAME_TITLE);
        //设置窗口大小
        setSize(FRAME_WIDTH, FRAME_HEIGHT);

        //设置窗口左上角坐标
        setLocation(FRAME_x, FRAME_y);

        //设置窗口不可缩放
        setResizable(false);

        //设置窗口可见
        setVisible(true);

        //求标题栏高度
        titleBarH = getInsets().top;
    }

    /**
     * 是Frame类的方法，继承下来的方法，该方法负责了所有的绘制内容，所有需要在屏幕中显示的内容，都要在该方法内调用
     * 该方法不能主动调用，必须通过调用repaint();去回调用该方法。
     * @param g1 the specified Graphics window
     */
    public void update(Graphics g1) {
        //第二步： 得到图片的画笔
        Graphics g = bufImg.getGraphics();
        //第三步：用图片画笔将所有内容绘制到图片上
        g.setFont(GAME_FONT);
        switch(gameState) {
            case STATUS_MENU:
                drawMenu(g);
                break;
            case STATUS_ABOUT:
                drawAbout(g);
                break;
            case STATUS_HELP:
                drawHelp(g);
                break;
            case STATUS_RUN:
                drawRun(g);
                break;
            case STATUS_OVER:
                drawOver(g);
                break;
        }

        //第四步：使用系统画笔，将图片绘制到Frame上
        g1.drawImage(bufImg, 0, 0, null);
    }

    /**
     * 绘制菜单状态下的内容
     * @param g 画笔对象
     */
    private void drawMenu(Graphics g) {
        //绘制黑色背景
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, FRAME_WIDTH, FRAME_HEIGHT);

        final int STR_WIDTH = 96;
        //菜单行间距
        final int rowHeight = 50;
        int x = FRAME_WIDTH - STR_WIDTH >> 1;
        int y = FRAME_HEIGHT / 3;
        g.setColor(Color.white);
        for (int i = 0; i < MENUS.length; i++) {
            if (i == menuIndex) {
                g.setColor(Color.RED);
            } else {
                g.setColor(Color.white);
            }
            g.drawString(MENUS[i], x, y + rowHeight * i);
        }
    }


    private void drawHelp(Graphics g) {
    }

    //游戏运行状态的绘制内容
    private void drawRun(Graphics g) {
        g.setColor(Color.black);
        g.fillRect(0, 0, FRAME_WIDTH, FRAME_HEIGHT);
        drawEnemies(g);
        myTank.draw(g);

        drawExplodes(g);
        //子弹和坦克碰撞的方法
        bulletCollideTank();
    }

    //绘制所有的敌人的坦克
    private void drawEnemies(Graphics g){
        for (int i = 0; i < enemies.size(); i++) {
            Tank enemy = enemies.get(i);
            enemy.draw(g);
        }
    }

    private void drawOver(Graphics g) {
    }

    private void drawAbout(Graphics g) {
    }

    /**
     * 初始化窗口事件监听
     */
    private void initListener() {
        //注册监听事件
        addWindowListener(new WindowAdapter() {
            //点击窗口关闭按钮时，此方法被调用
            @Override
            public void windowClosing(WindowEvent e) {
                //super.windowClosing(e);
                System.exit(0);
            }
        });

        //添加按键监听事件
        addKeyListener(new KeyAdapter() {
            //键盘按键按下时会回调
            @Override
            public void keyPressed(KeyEvent e) {
                int keyCode = e.getKeyCode();
                //不同的游戏状态，给出不同的处理方法
                switch(gameState) {
                    case STATUS_MENU:
                        keyPressEventMenu(keyCode);
                        break;
                    case STATUS_ABOUT:
                        keyPressEventAbout(keyCode);
                        break;
                    case STATUS_HELP:
                        keyPressEventHelp(keyCode);
                        break;
                    case STATUS_RUN:
                        keyPressEventRun(keyCode);
                        break;
                    case STATUS_OVER:
                        keyPressEventOver(keyCode);
                        break;
                }
            }

            //按键松开时回调
            @Override
            public void keyReleased(KeyEvent e) {
                int keyCode = e.getKeyCode();
                //不同的游戏状态，给出不同的处理方法
                if (gameState == STATUS_RUN) {
                    keyReleasedEventRun(keyCode);
                }
            }
        });
    }
    //菜单状态下的按键处理
    private void keyPressEventMenu(int keyCode) {
        switch (keyCode) {
            case KeyEvent.VK_UP:
            case KeyEvent.VK_W:
                menuIndex--;
                if (menuIndex < 0) {
                    menuIndex = MENUS.length - 1;
                }
                break;
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_S:
                menuIndex++;
                if (menuIndex > MENUS.length - 1) {
                    menuIndex = 0;
                }
                break;

            case KeyEvent.VK_ENTER:
                newGame();
                break;


        }
    }

    private void keyPressEventAbout(int keyCode) {

    }
    private void keyPressEventHelp(int keyCode) {

    }

    /**
     * 游戏运行中按键处理方法
     * @param keyCode
     */
    private void keyPressEventRun(int keyCode) {
        switch(keyCode) {
            case KeyEvent.VK_UP:
            case KeyEvent.VK_W:
                myTank.setDir(Tank.DIR_UP);
                myTank.setState(Tank.STATE_MOVE);
                break;
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_S:
                myTank.setDir(Tank.DIR_DOWN);
                myTank.setState(Tank.STATE_MOVE);
                break;
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_A:
                myTank.setDir(Tank.DIR_LEFT);
                myTank.setState(Tank.STATE_MOVE);
                break;
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_D:
                myTank.setDir(Tank.DIR_RIGHT);
                myTank.setState(Tank.STATE_MOVE);
                break;
            case KeyEvent.VK_SPACE:
                myTank.fire();
                break;
        }
    }

    /**
     * 按键松开时，游戏中处理方法
     * @param keyCode
     */
    private void keyReleasedEventRun(int keyCode) {
        switch(keyCode) {
            case KeyEvent.VK_UP:
            case KeyEvent.VK_W:
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_S:
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_A:
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_D:
                myTank.setState(Tank.STATE_STAND);
        }
    }
    private void keyPressEventOver(int keyCode) {

    }

    @Override
    public void run() {
        while (true) {
            repaint();
            try {
                Thread.sleep(REPAINT_INTERVAL);
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    //敌人的坦克子弹和我的坦克碰撞
    //我的坦克子弹和所有敌人的坦克碰撞
    private void bulletCollideTank() {
        //敌人的坦克子弹和我的坦克碰撞
        for (Tank enemy : enemies) {
            enemy.collideBullets(myTank.getBullets());
        }
        //我的坦克子弹和所有敌人的坦克碰撞
        for (Tank enemy : enemies) {
           myTank.collideBullets(enemy.getBullets());
        }
    }


    private void drawExplodes(Graphics g) {
        //所有敌人坦克身上的爆炸效果
        for (Tank enemy : enemies) {
            enemy.drawExplode(g);
        }
        //我的坦克身上的爆炸效果
        myTank.drawExplode(g);
    }

    /**
     * 开始新游戏的方法
     */
    private void newGame() {
        gameState = STATUS_RUN;
        //创建坦克对象、敌人的坦克对象
//        myTank = new Tank(400, 200, Tank.DIR_DOWN); // 抽像类不能实例化，应该实例化其实现类
        myTank = new MyTank(400, 200, Tank.DIR_DOWN);

        //使用一个单独的线程，用于生产敌人的坦克
        new Thread() {
            @Override
            public void run() {
                while (true) {
                    if (enemies.size() < ENEMY_MAX_COUNT){
                        Tank enemy = EnemyTank.createEmeny();
                        enemies.add(enemy);
                    }
                    try {
                        Thread.sleep(ENEMY_BORN_INTERVAL);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }
}
