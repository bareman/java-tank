package com.cypro.tank;

import com.cypro.util.MyUtil;

import java.awt.*;

/**
 * 自己坦克类，继承自Tank类
 */
public class MyTank extends Tank{

    private static Image[] tankImg;
    static{
        tankImg = new Image[4];
        tankImg[0] = MyUtil.createImage("res/u.png");
        tankImg[1] = MyUtil.createImage("res/d.png");
        tankImg[2] = MyUtil.createImage("res/l.png");
        tankImg[3] = MyUtil.createImage("res/r.png");
    }
    public MyTank(int x, int y, int dir) {
        super(x, y, dir);
    }

    @Override
    public void drawImageTank(Graphics g) {
        g.drawImage(tankImg[getDir()], getX() - RADIUS, getY() - RADIUS, null );
    }
}
