package com.cypro.tank;

import com.cypro.game.GameFrame;
import com.cypro.util.Constant;
import com.cypro.util.MyUtil;

import java.awt.*;

/**
 * 敌人坦克类，继承自Tank类
 */
public class EnemyTank extends Tank{
    //坦克图片数组
    private static Image[] enemyImg;

    //记录5秒开始的时间
    private long aiTime;

    //静态代码块对坦克图片数组进行初始化
    static{
        enemyImg = new Image[4];
        enemyImg[0] = MyUtil.createImage("res/ul.png");
        enemyImg[1] = MyUtil.createImage("res/dl.png");
        enemyImg[2] = MyUtil.createImage("res/ll.png");
        enemyImg[3] = MyUtil.createImage("res/rl.png");
    }

    public EnemyTank(int x, int y, int dir) {
        super(x, y, dir);
        //敌人一创建就计时
        aiTime = System.currentTimeMillis();
    }

    @Override
    public void drawImageTank(Graphics g) {
        ai();
        g.drawImage(enemyImg[getDir()], getX() - RADIUS, getY() - RADIUS, null );
    }

    //用于创建一个敌人的坦克
    public static Tank createEmeny() {
        int x = MyUtil.getRandomNumber(0, 2) == 0 ? RADIUS : Constant.FRAME_WIDTH - RADIUS;
        int y = GameFrame.titleBarH + RADIUS;
        int dir = DIR_DOWN;
        Tank enemy = new EnemyTank(x, y, dir);
        enemy.setEnemy(true);
        //TODO
        enemy.setState(STATE_MOVE);
        return enemy;
    }

    private void ai() {
        if(System.currentTimeMillis() - aiTime > Constant.ENEMY_AI_INTERVAL) {
            //间隔5秒随机一个状态
            setDir(MyUtil.getRandomNumber(DIR_UP, DIR_RIGHT + 1));
            setState(MyUtil.getRandomNumber(0, 2) == 0 ? STATE_STAND : STATE_MOVE);
            aiTime = System.currentTimeMillis();
        }
        //比较小的概率去开火
        if (Math.random() < Constant.ENEMY_FIRE_PERCENT) {
            fire();
        }
    }
}
