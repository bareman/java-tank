package com.cypro.tank;

import com.cypro.game.Bullet;
import com.cypro.game.Explode;
import com.cypro.game.GameFrame;
import com.cypro.util.BulletsPool;
import com.cypro.util.Constant;
import com.cypro.util.ExplodesPool;
import com.cypro.util.MyUtil;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 坦克类
 */
public abstract class Tank {
    public static final int DIR_UP = 0;
    public static final int DIR_DOWN = 1;
    public static final int DIR_LEFT = 2;
    public static final int DIR_RIGHT = 3;
    //坦克身体半径
    public static final int RADIUS = 30;
    //默认速度 每帧30ms
    public static final int DEFAULT_SPEED = 4;
    //坦克状态常量
    public static final int STATE_STAND = 0;
    public static final int STATE_MOVE = 1;
    public static final int STATE_DIE = 2;
    public static final int STATE_HP = 2;
    private int x, y;
    private int hp = STATE_HP;
    private int atk;
    private int speed = DEFAULT_SPEED;
    private int dir;
    private int state = STATE_STAND;
    private Color color;
    private boolean isEnemy = false;

    //炮弹
    private List<Bullet> bullets = new ArrayList();

    //用一个容器装载爆炸效果（多个子弹打在坦克身上都有爆炸效果）
    private List<Explode> explodes = new ArrayList<>();

    public Tank(int x, int y, int dir) {
        this.x = x;
        this.y = y;
        this.dir = dir;
        color= MyUtil.getRandomColor();
    }

    /**
     * 绘制坦克
     * @param g
     */
    public void draw(Graphics g) {
        logic();
        drawImageTank(g);
        //坦克发射炮弹
        drawBullets(g);
    }

    /**
     * 使用图片显示坦克
     * @param g
     */
     public abstract void drawImageTank(Graphics g);

    /**
     * 使用系统的方法绘制坦克
     * @param g
     */
    private void drawTank(Graphics g) {
        g.setColor(color);
        g.fillOval(x - RADIUS, y - RADIUS, RADIUS << 1, RADIUS << 1);
        int endX = x;
        int endY = y;
        switch(dir) {
            case DIR_UP:
                endY = y - RADIUS * 2;
                g.drawLine(x - 1, y, endX - 1, endY);
                g.drawLine(x + 1, y, endX + 1, endY);
                break;
            case DIR_DOWN:
                endY = y + RADIUS * 2;
                g.drawLine(x - 1, y, endX - 1, endY);
                g.drawLine(x + 1, y, endX + 1, endY);
                break;
            case DIR_LEFT:
                endX = x - RADIUS * 2;
                g.drawLine(x, y - 1, endX, endY - 1);
                g.drawLine(x + 1, y + 1, endX, endY + 1);
                break;
            case DIR_RIGHT:
                endX = x + RADIUS * 2;
                g.drawLine(x + 1, y - 1, endX + 1, endY - 1);
                g.drawLine(x + 1, y + 1, endX + 1, endY + 1);
                break;
        }
    }

    /**
     * 坦克的每一帧处理
     * @return
     */
    private void logic() {
        switch (state) {
            case STATE_STAND:
                break;
            case STATE_MOVE:
                move();
                break;
            case STATE_DIE:
                break;
        }
    }

    private void move() {
        switch (dir) {
            case DIR_UP:
                y -= speed;
                if (y < RADIUS + GameFrame.titleBarH) {
                    y = RADIUS +  + GameFrame.titleBarH;
                }
                break;
            case DIR_DOWN:
                y += speed;
                if (y > Constant.FRAME_HEIGHT - RADIUS) {
                    y = Constant.FRAME_HEIGHT - RADIUS;
                }
                break;
            case DIR_LEFT:
                x -= speed;
                if (x < RADIUS) {
                    x = RADIUS;
                }
                break;
            case DIR_RIGHT:
                x += speed;
                if (x > Constant.FRAME_WIDTH - RADIUS) {
                    x = Constant.FRAME_WIDTH - RADIUS;
                }
                break;
        }
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getAtk() {
        return atk;
    }

    public void setAtk(int atk) {
        this.atk = atk;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getDir() {
        return dir;
    }

    public void setDir(int dir) {
        this.dir = dir;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public List getBullets() {
        return bullets;
    }

    public void setBullets(List bullets) {
        this.bullets = bullets;
    }

    public boolean isEnemy() {
        return isEnemy;
    }

    public void setEnemy(boolean enemy) {
        isEnemy = enemy;
    }

    /**
     * 坦克开火方法
     * 创建了一个子弹对象，子弹的属性信息通过坦克的信息获得
     * 然后将创建的子弹信息添加到坦克管理的容器中
     */
    public void fire() {
        int bulletX = x - 14;
        int bulletY = y - 14;
        switch (dir) {
            case DIR_UP:
                bulletY -= RADIUS;
                break;
            case DIR_DOWN:
                bulletY += RADIUS;
                break;
            case DIR_LEFT:
                bulletX -= RADIUS;
                break;
            case DIR_RIGHT:
                bulletX += RADIUS;
                break;
        }
        Bullet bullet = BulletsPool.get();
        bullet.setX(bulletX);
        bullet.setY(bulletY);
        bullet.setAtk(dir);
        bullet.setDir(dir);
        bullet.setColor(color);
        bullet.setVisible(true);
        //改为从对像池中获取后，不再用下面这种方式了。
//        Bullet bullet = new Bullet(bulletX, bulletY, dir, atk, color);
        bullets.add(bullet);
    }

    /**
     * 将当前坦克发射的所有子弹绘制出来
     * @param g
     */
    private void drawBullets(Graphics g) {
        for(Bullet bullet: bullets) {
            bullet.draw(g);
        }
        // 打印容器中子弹的个数
//        System.out.println(bullets.size());

        //遍历所有的子弹，将不可见的子弹移除，并还原回对象池
        for (int i = 0; i < bullets.size(); i++) {
            Bullet bullet = bullets.get(i);
            if (!bullet.getVisible()) {
                Bullet remove = bullets.remove(i);
                BulletsPool.theReturn(remove);
            }
        }
//        System.out.println("坦克的子弹数量：" + bullets.size());
    }

    //坦克和敌人的子弹碰撞的方法
    public void collideBullets(List<Bullet> bullets) {
        //遍历所有的子弹和当前的坦克进行碰撞的检测
        for (Bullet bullet : bullets) {
            int beatX = bullet.getX(); //爆炸位置的x坐标
            int beatY = bullet.getY(); //爆炸位置的y坐标
            //子弹和坦克碰上了
            if (MyUtil.isCollide(x, y, RADIUS, beatX, beatY)) {
                //1. 子弹消失
                bullet.setVisible(false);
                //2. 坦克受到伤害
                //3. 添加爆炸效果
                Explode explode = ExplodesPool.get();
                explode.setX(x - RADIUS / 2 + 4);
                explode.setY(y + RADIUS);
                explodes.add(explode);
            }
        }
    }

    //绘制当前坦克身上所有的爆炸效果
    public void drawExplode(Graphics g) {
        System.out.println(explodes.size());
        for (Explode explode : explodes) {
            explode.draw(g);
            System.out.println(explode);
        }
    }

}
