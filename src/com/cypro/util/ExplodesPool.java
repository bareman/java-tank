package com.cypro.util;

import com.cypro.game.Bullet;
import com.cypro.game.Explode;

import java.util.ArrayList;
import java.util.List;

/**
 * 爆炸对象池类
 */
public class ExplodesPool {

    public static final int DEFAU_POOL_SIZE = 10;
    public static final int DEFAU_POOL_MAX_SIZE = 20;
    // 用于保存所有爆炸效果的容器
    private static List<Explode> pool = new ArrayList<>();

    //在类加载的时候，创建20个爆炸对象到容器中
    static {
        for (int i = 0; i < DEFAU_POOL_SIZE; i++) {
            pool.add(new Explode());
        }
    }
    //从池子中获取一个爆炸对象
    public static Explode get() {
        Explode explode = null;
        //池子中没有爆炸对象了
        if (pool.size() == 0) {
            explode = new Explode();
        } else {
            //如果池子中还有，则取第一个爆炸对象
            explode = pool.remove(0);
        }
        return explode;
    }

    //爆炸对象被销毁的时候，归还到爆炸池（对象池）
    public static void theReturn(Explode explode) {
        //爆炸池中数量达到最大值，则不归还了
        if (pool.size() == DEFAU_POOL_MAX_SIZE) {
            return;
        }
        pool.add(explode);
    }
}
