package com.cypro.util;

import com.cypro.game.Bullet;

import java.util.ArrayList;
import java.util.List;

/**
 * 子弹对象池类
 */
public class BulletsPool {
    public static final int DEFAU_POOL_SIZE = 200;
    public static final int DEFAU_POOL_MAX_SIZE = 300;
    // 用于保存所有的子弹的容器
    private static List<Bullet> pool = new ArrayList<>();

    //在类加载的时候，创建200个子弹对象到容器中
    static {
        for (int i = 0; i < DEFAU_POOL_SIZE; i++) {
            pool.add(new Bullet());
        }
    }
    //从池子中获取一个子弹对象
    public static Bullet get() {
        System.out.println("从对象池中获取了一个对象，剩余：" + pool.size());
        Bullet bullet = pool.get(0);
        //池子中没有子弹了
        if (pool.size() == 0) {
            bullet = new Bullet();
        } else {
            //如果池子中还有，则取第一个子弹对象
            bullet = pool.remove(0);
        }
        return bullet;
    }

    //子弹被销毁的时候，归还到子弹池（对象池）
    public static void theReturn(Bullet bullet) {
        //子弹池中数量达到最大值，则不归还了
        if (pool.size() == DEFAU_POOL_MAX_SIZE) {
            return;
        }
        pool.add(bullet);
        System.out.println("对象池中归还了一个子弹对象，当前池中的数量为：" + pool.size());
    }
}
