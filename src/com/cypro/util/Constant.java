package com.cypro.util;
import java.awt.*;
/**
 * 游戏中的常量都在该类中维护，方便后期的管理
 */
public class Constant {
    public static Toolkit toolkit = Toolkit.getDefaultToolkit();
    public static Dimension screenSize = toolkit.getScreenSize();

    public static int screenWidth = screenSize.width;
    public static int screenHeight = screenSize.height;
    public static final String GAME_TITLE = "坦克大战";
    public static final int FRAME_WIDTH = 400;
    public static final int FRAME_HEIGHT = 400;

    public static final int FRAME_x = (screenWidth - FRAME_WIDTH) >> 1;
    public static final int FRAME_y = (screenHeight - FRAME_HEIGHT) >> 1;


    /*********************** 游戏菜单相关 *****************************/
    public static final int STATUS_MENU = 0;
    public static final int STATUS_HELP = 1;
    public static final int STATUS_ABOUT = 2;
    public static final int STATUS_RUN = 3;
    public static final int STATUS_OVER = 4;

    public static final String[] MENUS = {
            "开始游戏",
            "继续游戏",
            "游戏帮助",
            "游戏关于",
            "退出游戏"
    };

    //字体
    public static final Font GAME_FONT = new Font("宋体", Font.BOLD, 24);

    //刷新间隔
    public static final int REPAINT_INTERVAL = 30;

    //最大敌人坦克的数量
    public static final int ENEMY_MAX_COUNT = 10;

    //敌人坦克产生的时间间隔
    public static final int ENEMY_BORN_INTERVAL = 5000;

    //敌人切换状态的时间间隔
    public static final int ENEMY_AI_INTERVAL = 3000;

    //敌人开火的百分率
    public static final double ENEMY_FIRE_PERCENT = 0.03;



}
