package com.cypro.util;

import java.awt.*;

/**
 * 工具类
 */
public class MyUtil {
    private MyUtil(){}

    public static final int getRandomNumber(int min, int max) {
        return (int)(Math.random() * (max - min) + min);
    }

    /**
     * 生成随机颜色
     * @return
     */
    public static final Color getRandomColor() {
        int red = getRandomNumber(0, 256);
        int green = getRandomNumber(0, 256);
        int blue = getRandomNumber(0, 256);
        return new Color(red, green, blue);
    }

    /**
     * 判断一个是否在一个正方形内部
     * @param rectX 正方形中心点x坐标
     * @param rectY 正方形中心点y坐标
     * @param radius 正方形边长的一半
     * @param pointx 子弹x坐标
     * @param pointY 子弹y坐标
     * @return 如果在正方形内部，返回true, 否则返回false
     */
    public static final boolean isCollide(int rectX, int rectY, int radius, int pointX, int pointY){
        //正方形中心点和子弹的x,y轴的距离
        int disX = Math.abs(rectX - pointX);
        int disY = Math.abs(rectY - pointY);
        return disX < radius && disY < radius;
    }

    /**
     * 根据图片资源路径创建和加载图片对象
     * @param path 图片路径
     * @return
     */
    public static final Image createImage(String path) {
        return Toolkit.getDefaultToolkit().createImage(path);
    }
}
